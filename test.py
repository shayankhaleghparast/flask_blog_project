from flask import Flask,render_template,request,redirect,session
from flask_sqlalchemy import SQLAlchemy
import hashlib, uuid
import os

app = Flask(__name__) # global fask object

#connect to database
app.config['SQLALCHEMY_DATABASE_URI'] ='mysql+pymysql://laravel:laravel@localhost/ie_proj'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.secret_key = os.urandom(52345)
db = SQLAlchemy(app)

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), nullable=False)
    content = db.Column(db.String(600),nullable=False)
    date = db.Column(db.String(120), nullable=False ,default="1 مهر 96")
    tags = db.Column(db.String(120), nullable=True)
    author_id = db.Column(db.Integer,db.ForeignKey('user.id'),nullable=False)
    category_id = db.Column(db.Integer,db.ForeignKey('category.id'),nullable=False)

class Course(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), nullable=False)
    content = db.Column(db.String(550), nullable=False)
    semester = db.Column(db.Integer, nullable=False)
    author_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), nullable=False)
    email = db.Column(db.String(120), nullable=False)
    role = db.Column(db.String(120), nullable=False)
    password = db.Column(db.String(120),nullable=False)
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    courses = db.relationship('Course', backref='author', lazy='dynamic')

class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), nullable=False)
    description = db.Column(db.String(120), nullable=False)
    posts = db.relationship('Post', backref='category', lazy='dynamic')

#line 28 to 40 sholud be run for the first time then you can comment it

# db.drop_all()
# db.create_all()

# #use this three users for your user table.for adding new post you can use blog add post button in homepage
# ali = User(username="علی حسابی",email="ali@gmail.com",password="12341234")
# db.session.add(ali)

# reza = User(username="یوسف اسفندیاری",email="reza@gmail.com",password="12341234")
# db.session.add(reza)

# simti = User(username="simti",email="simti@gmail.com",password="12341234")
# db.session.add(simti)

# db.session.commit();


@app.route('/')
def index():
    # return 'home page'
    return render_template('index.html',posts=Post.query.order_by(Post.date.desc()).limit(5),session=session)

@app.route('/dashboard')
def dashboard():
    if not('username' in session):
        return redirect('/')
    logged_in_user = User.query.filter(User.username == session['username']).first()
    return render_template('dashboard.html',session=session,user = logged_in_user)

@app.route('/semesters') 
def semesters():
    if not('username' in session):
        return redirect('/')
    return render_template('semesters.html', session=session)
       

@app.route('/semester/<semester>')
def semester(semester):
    courses = Course.query.filter(Course.semester == semester).all()
    return render_template('semester.html', courses=courses)

@app.route('/course/<id>')
def course(id):
    courses = Course.query.get(id)
    return render_template('course.html', course=course)


@app.route('/post/<id>')
def post(id):
    return render_template('post.html',post = Post.query.get(id))

@app.route('/about')
def about():
    return render_template('about.html')
    
@app.route('/new_post',methods=["GET","POST"]) 
def new_post():
    if not('username' in session):
        return redirect('/')
    if request.method == "GET":
        return render_template('add_post.html',categories = Category.query.all())
    else:
        title = request.form['title']
        category_id = 1
        author_id = request.form['author_id']
        date = "1 مهر 96"
        content = request.form['content']
        tags = request.form['tags']
        db.session.add(Post(title=title,content=content,date=date,category_id=category_id,tags=tags,author_id=author_id))
        db.session.commit();
        return redirect("http://127.0.0.1:8080/", code=302)
       

@app.route('/register',methods=["GET","POST"]) 
def register():
    if request.method == "GET":
        return render_template('register.html')
    else:
        username = request.form['username']
        email = request.form['email']
        password = request.form['password']
        db.session.add(User(username=username,email=email,password=password,role="student"))
        db.session.commit();
        session['username'] = username
        session['password'] = password
        session['email'] = email
        return redirect("http://127.0.0.1:8080/", code=302)


@app.route('/login',methods=["GET","POST"])
def login():
    username = request.form['username']
    password = request.form['password']
    user = User.query.filter_by(username=username,password=password).first()
    if user is None:
        return redirect("http://127.0.0.1:8080/", code=302)
    session['username'] = username
    session['password'] = password
    return redirect("http://127.0.0.1:8080/dashboard", code=302)

    
@app.route('/logout')
def logout():
    session.pop('username',None)
    return redirect("http://127.0.0.1:8080", code=302)
#end routes

if __name__ == '__main__':
    app.run(port=8080, debug=True)